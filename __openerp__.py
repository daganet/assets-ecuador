# -*- coding: utf-8 -*-
#######################################
#
# Account Assets Ecuador
# Copyright (C) 2012 DAGANET WEB SOLUTIONS
# All rights reserved.
#
# This software is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.
#
# Authors: Dairon Medina <dairon@daganet.net>
#
#######################################

{
    "name" : "Account Assets Ecuador",
    "version" : "0.1",
    "licence": "GPL3",
    "author" : "DaGaNET Web & Open Source Solutions",
    "website" : "http://www.daganet.net",
    "category" : "Added functionality/Asset Extension",
    "depends" : ['base','account_asset'],
    "description": """
    Add new fiels for Ecuador Assets Management.
    """,
    "init_xml": [],
    "update_xml": [
        #'security/ir.model.access.csv',
        'views/asset_ec_view.xml',
    ],
    "auto_install": False,
    "installable": True,
    "application": False,

}

