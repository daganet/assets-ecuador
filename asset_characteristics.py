# -*- coding: utf-8 -*-
#######################################
#
# Account Assets Ecuador
# Copyright (C) 2012 DAGANET WEB SOLUTIONS
# All rights reserved.
#
# This software is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.
#
# Authors: Dairon Medina <dairon@daganet.net>
#
#######################################
from osv import osv, fields
from tools.translate import _

class asset_caracteristics(osv.osv):
    """
    Group of asset characteristics
    """
    _name = 'asset.characteristics'
    _description = __doc__

    _columns = {
        'name' : fields.char('Characteristic', size=64, required=True),
        'value' : fields.char('Value', size=255),
        'asset_id': fields.many2one('account.asset.asset', 'Asset'),
    }
asset_caracteristics()