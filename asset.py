# -*- coding: utf-8 -*-
#######################################
#
# Account Assets Ecuador
# Copyright (C) 2012 DAGANET WEB SOLUTIONS
# All rights reserved.
#
# This software is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.
#
# Authors: Dairon Medina <dairon@daganet.net>
#
#######################################
from osv import osv,fields
import decimal_precision as dp
from tools.translate import _



class account_asset_brands(osv.osv):
    """
    Assets Brands
    """
    _name = 'asset.brands'
    _description = __doc__
    _table = 'asset_brands'

    _columns = {
        'name': fields.char('Brand', size=255, required=True),
    }
account_asset_brands()

class account_asset_models(osv.osv):
    """
    Assets Models
    """
    _name = 'asset.models'
    _description = __doc__
    _table = 'asset_models'

    _columns = {
        'brand_id': fields.many2one('asset.brands', 'Brand', required=True),
        'name': fields.char('Model', size=255, required=True),
    }

account_asset_models()

class asset_conservation_state(osv.osv):
    """
    Assets Conservation State
    """
    _name = 'asset.conservation.state'
    _description = __doc__
    _table = 'asset_conservation_state'

    _columns = {
        'name': fields.char('Conservation State', size=255, required=True),
        'description': fields.text('Description'),
    }
asset_conservation_state()

class asset_operative_status(osv.osv):
    """
    Assets Operative Status
    """
    _name = 'asset.operative.status'
    _description = __doc__
    _table = 'asset_operative_status'

    _columns = {
        'name': fields.char('Operative Status', size=255, required=True),
        'description': fields.text('Description'),
    }
asset_operative_status()

class account_asset_asset(osv.osv):
    _inherit = "account.asset.asset"

    _columns = {
        'anterior_code': fields.char('Previous Code', size=100),
        'reference_code': fields.char('Reference Code', size=100),
        'serial': fields.char('Serial', size=100, required=True),
        'taken_by': fields.char('Taken By', size=255),
        'taken_date': fields.date('Taken Date'),
        'quantity': fields.integer('Quantity'),
        'fabrication_date': fields.date('Fabrication Date'),
        'operation_date': fields.date('Operation date'),
        'barcode': fields.char('Barcode', size=50),
        'brand':fields.many2one('asset.brands', 'Brand'),
        'operative_status': fields.many2one('asset.operative.status', 'Operative Status'),
        'conservation_state': fields.many2one('asset.conservation.state', 'Conservation State'),
        'characteristic_ids': fields.one2many('asset.characteristics', 'asset_id', 'Characteristics')
    }

account_asset_asset()
